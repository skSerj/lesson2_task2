package com.sourceit.lesson2.task2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText login;
    TextView loginError;
    EditText email;
    TextView emailError;
    EditText phoneNumber;
    TextView phoneNumberError;
    EditText password;
    TextView passwordFieldEmptyError;
    EditText confirmPassword;
    TextView confirmPasswordError;
    ImageView validSignIn;

    Button signIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.login);
        loginError = findViewById(R.id.login_error);
        email = findViewById(R.id.email);
        emailError = findViewById(R.id.email_error);
        phoneNumber = findViewById(R.id.phone_number);
        phoneNumberError = findViewById(R.id.phone_number_error);
        password = findViewById(R.id.password);
        passwordFieldEmptyError = findViewById(R.id.password_field_isEmpty_error);
        confirmPassword = findViewById(R.id.confirm_password);
        confirmPasswordError = findViewById(R.id.confirm_password_error);


        signIn = findViewById(R.id.button);

        validSignIn = findViewById(R.id.hello_android);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkErrors()) {
                    validSignIn.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean checkErrors() {
        boolean hasError = false;
        if (login.getText().toString().isEmpty()) {
            hasError = true;
            loginError.setVisibility(View.VISIBLE);
        } else {
            loginError.setVisibility(View.GONE);
        }

        if(!email.getText().toString().contains("@")){
            hasError = true;
            emailError.setVisibility(View.VISIBLE);
        }else {
            emailError.setVisibility(View.GONE);
        }

        if (!(phoneNumber.getText().toString().contains("+380") && phoneNumber.getText().toString().length() == 13)) {
            hasError = true;
            phoneNumberError.setVisibility(View.VISIBLE);
        } else {
            phoneNumberError.setVisibility(View.GONE);
        }

        if (password.getText().toString().isEmpty()) {
            hasError = true;
            passwordFieldEmptyError.setVisibility(View.VISIBLE);
        } else {
            passwordFieldEmptyError.setVisibility(View.GONE);
        }

        if (!(confirmPassword.getText().toString().equals(password.getText().toString()))) {
            hasError = true;
            confirmPasswordError.setVisibility(View.VISIBLE);
        } else {
            confirmPasswordError.setVisibility(View.GONE);
        }


        return hasError;
    }
}
